import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { simpleReducer } from './simple.reducer';
import { postReducer } from './complexngrx/post.reducer';
import { TodoReducer } from './basics/reducer/todo.reducer';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { reducers } from './basics/reducer';
import { TypeformWidget } from 'typeform-elements';
import { LoginComponent } from './login/login.component';
import { ForgotpassComponent } from './forgotpass/forgotpass.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotpassComponent
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),

  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
