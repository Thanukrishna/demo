import * as PostActions from '../complexngrx/post.action';
import { Post } from '../complexngrx/post.model';
import { Action } from '@ngrx/store';
export type tion = PostActions.All;

const defaultstate: Post = {
    text: "Hii everyone my new app",
    likes: 0
}

const newstate = (state: any, newData: any) => {
    return Object.assign({}, state, newData);
}

export function postReducer(state: Post = defaultstate, action: tion) {
    console.log(action.type, state)

    switch (action.type) {
        case PostActions.EDIT_TEXT:
            return newstate(state, { text: action.payload });
        case PostActions.UPVOTE:
            return newstate(state, { likes: state.likes + 1 });
        case PostActions.DOWNVOTE:
            return newstate(state, { likes: state.likes - 1 });
        case PostActions.RESET:
            return defaultstate
        default:
            return state;
    }

}