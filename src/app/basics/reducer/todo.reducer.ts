import { TodoActiontypes } from "../shared/enum/todoactiontypes.enum";
import { Actionparent } from "../action/todo.action";
import { Todo } from "../model/todo";

const initialState: Todo[] = [
    { title: "todo1" }, { title: "todo2" }, { title: "todo3" },]


export function TodoReducer(state: Todo[] = initialState, action: Actionparent): Todo[] {
    switch (action.type) {
        default:
            return state;
    }

}