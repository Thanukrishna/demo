import { TodoReducer } from './todo.reducer';
import { Todo } from '../model/todo';
import { ActionReducerMap } from '@ngrx/store';


export const rootReducer = {};

export interface AppState {
    shoppingList: Todo[];
};


export const reducers: ActionReducerMap<AppState, any> = {
    shoppingList: TodoReducer
};