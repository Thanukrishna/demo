import { Action } from '@ngrx/store';
import { TodoActiontypes } from '../shared/enum/todoactiontypes.enum';

export class Actionparent implements Action {
    type: any;
    payload: any;
}
export class TodoAdd implements Action {
    type = TodoActiontypes.Add;
    constructor(public payload: any) { }
}