import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotpassComponent } from './forgotpass/forgotpass.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [{ path: "", redirectTo: "login", pathMatch: "full" },
{ path: "login", component: LoginComponent },
{ path: "forgotpassword", component: ForgotpassComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
