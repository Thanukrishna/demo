import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as typeformEmbed from '@typeform/embed'


export interface Appstate {
  message: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  message: any;
  constructor(private store: Store<Appstate>) {
    this.message = this.store.select('message')
  }
  english() {
    this.store.dispatch({ type: 'English' });
  }
  tamil() {
    this.store.dispatch({ type: 'Tamil' })
  }
  ngOnInit() {

  }
  ngAfterViewInit(): void {

  }
}
